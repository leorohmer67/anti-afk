package com.papyconfig.antiafk;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;

public final class AntiAFK extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic
        saveDefaultConfig();
        FileConfiguration config = getConfig();
        Check checkTask = new Check(this, config);
        getLogger().log(Level.CONFIG, "Plugin enabled");
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, checkTask, 0, config.getInt("chrono.looping"));
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        getLogger().log(Level.CONFIG, "Plugin disabled");
    }
}
